import 'package:flutter/material.dart';

class LargeCard extends StatelessWidget {
  final String text;

  LargeCard({this.text});

  @override
  Widget build(BuildContext context) {
    return new Card(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Text(this.text),
        ],
      ),
    );
  }
}
