import 'package:flutter/material.dart';

class SmallCard extends StatelessWidget {
  final String text;
  final Function callback;
  final String subTitle;

  SmallCard({
    this.text,
    this.subTitle,
    this.callback,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          InkWell(
            onTap: this.callback,
            child: ListTile(
              title: Text(this.text),
              subtitle: Text(this.subTitle),
            ),
          ),
        ],
      ),
    );
  }
}
