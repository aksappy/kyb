import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:kyb/reducers/reducers.dart';
import 'package:kyb/screens/home.dart';
import 'package:kyb/store/store.dart';
import 'package:kyb/utils/constants.dart';
import 'package:kyb/utils/router.dart';
import 'package:redux/redux.dart';

void main() => runApp(KnowYourBeerApp());

class KnowYourBeerApp extends StatelessWidget {
  final String title = 'KnowYourBeer';
  final store = new Store<BeerStore>(
    appReducer,
    initialState: new BeerStore(),
    middleware: [],
  );

  MaterialApp createApp() {
    return MaterialApp(
      title: 'KYB - Know Your Beer!',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      onGenerateRoute: Router.generateRoute,
      initialRoute: ROUTE_INIT,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new StoreProvider(
      store: store,
      child: createApp(),
    );
  }
}
