
import 'package:flutter/material.dart';
import 'package:kyb/store/store.dart';
import 'package:redux/redux.dart';

class LoginModel {
  final bool isFirstTime;

  LoginModel({@required this.isFirstTime});

  static LoginModel fromStore(Store<BeerStore> store) {
    print("Called");
    return new LoginModel(isFirstTime: store.state.isFirstLogin);
  }

}