import 'package:kyb/store/store.dart';

BeerStore appReducer(BeerStore store, action) {
  return new BeerStore(
    isFirstLogin: firstTimeLoginReducer(store, action),
  );
}

bool firstTimeLoginReducer(BeerStore store, action) {
  return !store.isFirstLogin;
}
