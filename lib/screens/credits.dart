import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class CreditScreen extends StatelessWidget {
  List<Widget> getCredits() {
    return null;
  }

  Widget createCard(
      Widget leading, String title, String subTitle, Function callback) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          InkWell(
            onTap: callback,
            child: ListTile(
              leading: leading,
              title: Text(title),
              subtitle: Text(subTitle),
            ),
          ),
        ],
      ),
    );
  }

  const CreditScreen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Credits"),
      ),
      body: new Container(
        child: new Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(2),
              padding: EdgeInsets.all(20),
              child: Text(
                "The following items were used in creating this application. Because of them, this app runs free",
                style: TextStyle(fontSize: 16),
              ),
            ),
            createCard(Image.asset("assets/images/beer.png"),
                'Icon made by Freepik', 'From flaticon', () {
              launch("http://www.freepik.com/");
            }),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {},
        tooltip: "Rate me in Play Store",
        label: Text("Rate me!"),
        icon: Icon(Icons.stars),
      ),
    );
  }
}
