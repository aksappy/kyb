import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:kyb/actions/actions.dart';
import 'package:kyb/models/login.dart';
import 'package:kyb/store/store.dart';
import 'package:redux/redux.dart';

class HomeScreen extends StatelessWidget {
  getData() {}

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      floatingActionButton: new ChangeButton(),
      body: Center(
        child: Row(
          children: <Widget>[
            FirstTimeLoginData()
          ],
        ),
      ),
    );
  }
}

class FirstTimeLoginData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new StoreConnector<BeerStore, LoginModel>(
      converter: LoginModel.fromStore,
      builder: (BuildContext context, LoginModel loginModel) {
        return new Text(
          loginModel.isFirstTime.toString(),
          style: TextStyle(
            color: Colors.black,
            fontSize: 48,
          ),
        );
      },
    );
  }
}

class ChangeButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new StoreConnector<BeerStore, VoidCallback>(
      builder: (BuildContext context, VoidCallback callback) {
        return new FloatingActionButton(
          onPressed: callback,
          child: Icon(Icons.add),
          backgroundColor: Colors.blue,
        );
      },
      converter: (Store<BeerStore> store) {
        return () {
          print("Clicked");
          store.dispatch(new FirstTimeLogin());
        };
      },
    );
  }
}
