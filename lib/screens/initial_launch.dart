import 'package:flutter/material.dart';

class InitialLaunchScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Colors.red,
              borderRadius: BorderRadius.all(
                Radius.elliptical(100,100),
              ),
            ),
            margin: EdgeInsets.all(40.0),
            child: Center(child: Text("Page 1")),
          ),
          Container(decoration: BoxDecoration(
              color: Colors.blueAccent,
              borderRadius: BorderRadius.all(
                Radius.elliptical(100,100),
              ),
            ),
            margin: EdgeInsets.all(20.0),
            child: Center(child: Text("Page 2")),
          )
        ],
        scrollDirection: Axis.horizontal,
        physics: BouncingScrollPhysics(),
        pageSnapping: false,
      ),
    );
  }
}
