class BeerStore {
  final bool isFirstLogin;

  BeerStore({this.isFirstLogin: false});

  BeerStore copyWith({int count, bool isLoading}) {
    return new BeerStore(isFirstLogin: isFirstLogin ?? this.isFirstLogin);
  }

  @override
  String toString() {
    return 'AppState{isLoading: $isFirstLogin}';
  }
}
