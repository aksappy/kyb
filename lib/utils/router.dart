import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:kyb/screens/credits.dart';
import 'package:kyb/screens/home.dart';
import 'package:kyb/screens/initial_launch.dart';
import 'package:kyb/utils/constants.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case ROUTE_HOME:
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case ROUTE_INIT:
        return MaterialPageRoute(builder: (_) => InitialLaunchScreen());
      case ROUTE_CREDITS:
        return MaterialPageRoute(builder: (_) => CreditScreen());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }
}
